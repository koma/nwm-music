<?php
/*
Server-side PHP file upload code for HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
 */

$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
$cat = (isset($_GET['category'])) ? $_GET['category'] : 'others';

if ($fn)
{
    // AJAX call
    file_put_contents(
        'uploads/' . $cat . '/' . $fn,
        file_get_contents('php://input')
    );
    echo "$fn uploaded in folder $cat";
    file_put_contents("uploads/uploads.txt", $cat.'/'.$fn."\n");
}
else
{
    // form submit
    $files = $_FILES['fileselect'];

    foreach ($files['error'] as $id => $err)
    {
        if ($err == UPLOAD_ERR_OK)
        {
            $fn = $files['name'][$id];
            move_uploaded_file(
                $files['tmp_name'][$id],
                'uploads/' . $cat . '/' . $fn
            );
            echo exec("find uploads/ -iname \"*.mp3\" > uploads/test.txt");
            /*echo "File $fn uploaded in folder $cat.";
            file_put_contents("uploads/uploads.txt", $cat.'/'.$fn."\n");*/
        }
    }
}
