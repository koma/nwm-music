
/*
   filedrag.js - HTML5 File Drag & Drop demonstration
*/
(function() {

    // getElementById
    function $id(id) {
        return document.getElementById(id);
    }

    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
        e.target.className = (e.type == "dragover" ? "hover" : "");
    }


    // file selection
    function FileSelectHandler(e) {

        // cancel event and hover styling
        FileDragHover(e);

        // fetch FileList object
        var files = e.target.files || e.dataTransfer.files;

        // process all File objects
        for (var i = 0, f; f = files[i]; i++)
            UploadFile(f);
    }

    function BytesToSize(bytes, precision)
    {
        var kilobyte = 1024;
        var megabyte = kilobyte * 1024;
        var gigabyte = megabyte * 1024;
        var terabyte = gigabyte * 1024;

        if ((bytes >= 0) && (bytes < kilobyte)) {
            return bytes + 'B';

        } else if ((bytes >= kilobyte) && (bytes < megabyte)) {
            return (bytes / kilobyte).toFixed(precision) + 'KB';

        } else if ((bytes >= megabyte) && (bytes < gigabyte)) {
            return (bytes / megabyte).toFixed(precision) + 'MB';

        } else if ((bytes >= gigabyte) && (bytes < terabyte)) {
            return (bytes / gigabyte).toFixed(precision) + 'GB';

        } else if (bytes >= terabyte) {
            return (bytes / terabyte).toFixed(precision) + 'TB';

        } else {
            return bytes + 'B';
        }
    }

    function UploadFile(file) {

        // following line is not necessary: prevents running on SitePoint servers
        if (location.host.indexOf("sitepointstatic") >= 0) return

            var xhr = new XMLHttpRequest();
        if (xhr.upload && (file.type == "audio/mp3" || file.type == "audio/mpeg" ||file.type == "audio/wav")
            && file.size <= $id("MAX_FILE_SIZE").value) {

                // create progress bar
                //var o = $id("progress");
                //var progress = o.appendChild(document.createElement("p"));
                //progress.appendChild(document.createTextNode("upload " + file.name));
                var obj = $id("progresstable");
                var row = obj.appendChild(document.createElement("tr"));
                var progress = row.appendChild(document.createElement("td"));
                progress.className = "progressbar";
                var title = row.appendChild(document.createElement("td"));
                title.appendChild(document.createTextNode(file.name));
                var size = row.appendChild(document.createElement("td"));
                size.appendChild(document.createTextNode(BytesToSize(file.size, 2)));


                // progress bar
                xhr.upload.addEventListener("progress", function(e) {
                    var pc = parseInt(100 - (e.loaded / e.total * 100));
                    progress.style.backgroundPosition = pc + "% 0";
                    progress.innerHTML = (100 - pc) + "%";
                }, false);

                // file received/failed
                xhr.onreadystatechange = function(e) {
                    //alert(progess.className);
                    if (xhr.readyState == 4) {
                        var res = (xhr.status == 200 ? "success" : "failure");
                        progress.className = res + " " + progress.className;
                        progress.innerHTML = res;
                    }
                };
                // start upload
                // console.log(cat);
                var cat = $id("category");
                var params = "category=" + cat.options[cat.selectedIndex].value;
                xhr.open("POST", $id("upload").action + "?" + params, true);
                //xhr.open("POST", $id("upload").action, true);
                xhr.setRequestHeader("X_FILENAME", file.name);
                xhr.send(file);
            }

    }


    // initialize
    function Init() {

        var fileselect = $id("fileselect"),
            filedrag = $id("filedrag"),
            submitbutton = $id("submitbutton");

        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);

        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {

            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            filedrag.addEventListener("drop", FileSelectHandler, false);
            filedrag.style.display = "block";

            // remove submit button
            submitbutton.style.display = "none";
        }

    }

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }


})();
